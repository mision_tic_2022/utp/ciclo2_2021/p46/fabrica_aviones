public class App {
    public static void main(String[] args) throws Exception {

        System.out.println("-----Avion De Carga----------");
        AvionCarga objAvionCarga = new AvionCarga("Azul", 450.0);
        String color = objAvionCarga.getColor();
        System.out.println(color);

        System.out.println("-----Avion Comercial----------");
        AvionPasajeros objAvionPasajeros = new AvionPasajeros(250, "Blanco", 200);
        objAvionPasajeros.despegar();
        objAvionPasajeros.servir();

        System.out.println("-----Avion Militar----------");
        AvionMilitar objAvionMilitar = new AvionMilitar(6, "Azul", 180);
        objAvionMilitar.despegar();
        objAvionMilitar.detectar_amenaza(false);
        objAvionMilitar.detectar_amenaza(true);
        objAvionMilitar.aterrizar();
        objAvionMilitar.frenar();

        
    }
}
