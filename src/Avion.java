public class Avion{
    /**************
     * Atributos
     **************/
    private String color;
    private double tamanio;

    /**************
     * Constructor
     *************/
    public Avion(String color, double tamanio){
        this.color = color;
        this.tamanio = tamanio;
    }

    public String getColor(){
        return this.color;
    }

    /********************
     * Métodos (acciones)
     ********************/
    public void aterrizar(){
        System.out.println("Aterrizando...");
    }
    public void despegar(){
        System.out.println("Despegando...");
    }
    public void frenar(){
        System.out.println("Frenando...");
    }
}