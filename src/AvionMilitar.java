public class AvionMilitar extends Avion{
    private int misiles;

    public AvionMilitar(int misiles, String color, double tamanio){
        super(color, tamanio);
        this.misiles = misiles;
    }

    public void detectar_amenaza(boolean amenaza){
        if(amenaza){
            this.disparar();
        }else{
            System.out.println("No es una amenaza");
        }
    }

    private void disparar(){
        if(this.misiles > 0){
            System.out.println("Disparando...");
            --this.misiles;
        }else{
            System.out.println("No hay munición");
        }
        
    }
}
